FROM resin/raspberrypi3-python:2.7

# Enable systemd init system in container
ENV INITSYSTEM=on

# Get Mopidy repo
RUN wget -q -O - https://apt.mopidy.com/mopidy.gpg | apt-key add - && \
    wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/mopidy.list
RUN apt-get clean && apt-get update

# Upgrade system
#RUN apt-get upgrade -yq

# Install binary dependencies and then cleanup
RUN apt-get install -yq --no-install-recommends \
        python-pip \
        python-cryptography \
        gstreamer1.0-alsa \
        gstreamer1.0-plugins-bad \
        libasound2-dev \
        libffi-dev \
        libssl-dev \
        libraspberrypi-bin
RUN apt-get install -yq --no-install-recommends \
        alsa-utils \
        flite \
        sox \
        git \
        mopidy \
        mopidy-spotify \
        mopidy-soundcloud && \
    rm -rf /var/lib/apt/lists/*

# Update setuptools (known working version: 27.1.2)
RUN pip install --upgrade setuptools==27.1.2

# Install Mopidy extensions
RUN pip install mopidy-gmusic Mopidy-YouTube Mopidy-Spotmop mopidy-musicbox-webclient

# Copy configuration files
COPY etc/asound.conf /etc/asound.conf
COPY etc/mopidy.conf /etc/mopidy/mopidy.conf

# Defines our working directory in container
WORKDIR /usr/src/app

# Copies the requirements.txt first for better cache on later pushes
COPY requirements.txt requirements.txt

# This install npm dependencies on the resin.io build server,
# making sure to clean up the artifacts it creates in order to reduce the image size.
RUN pip install -r requirements.txt

# This will copy all files in our root to the working  directory in the container
COPY . ./

# server.js will run when container starts up on the device
CMD ["bash", "start.sh"]
