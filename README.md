# Maia

This is the beginning of Maia - __M__y __A__rtifically __I__ntelligent __A__gent. It is a python-based project deployed using [Resin.io](https://resin.io).

## Setup

To get this project up and running, you will need to signup for a resin.io account [here](https://dashboard.resin.io/signup) and set up a device, have a look at our [Getting Started tutorial](http://docs.resin.io/raspberrypi/python/getting-started/). Once you are set up with resin.io, you will need to clone this repo locally:
```
$ git clone git@gitlab.com:apfritts/maia.git
```

Then add your resin.io application's remote repository to your local repository:
```
$ git remote add resin username@git.resin.io:username/myapp.git
```

and push the code to the newly added remote:
```
$ git push resin master
```

## Local Development

This project is a python2.7-based project. To get started, install or activate virtualenv:
```
virtualenv virtualenv && source ./virtualenv/bin/activate && pip install -r requirements.txt

OR

source ./virtualenv/bin/activate
```

This project must run on a Resin device as well as macOS. To do this, we have to carefully select which packages are required and how they are used, otherwise development in both environments will not be possible.

Run the local server:
```
./start.sh
```
