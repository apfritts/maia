var async = require('async');
var child_process = require('child_process');
var flite = require('flite');
var fs = require('fs');
var nodeAsound = require('node-asound');
var Sound = require('aplay');

var tts = null;
flite(function(err, speech) {
  if (err) {
    console.error('Failed to initialize flite');
    console.error(err);
  } else {
    console.log('Initialized flite TTS');
    tts = speech;

    // Turn up the volume
    child_process.exec("amixer sset 'Digital' 100%", function(err, stdout, stderr) {
      if (err) {
        console.log('ERROR SETTING VOLUME!');
        console.log(err);
        console.log(stdout);
        console.log(stderr);
      }
      processQueue();
    });
  }
});

function displayDevice(type, device) {
  if (device) {
    console.log({
      type: type,
      getCardNumber: device.getCardNumber(),
      getDeviceNumber: device.getDeviceNumber(),
      getName: device.getName(),
      getCardNumber: device.getCardNumber(),
    });
  } else {
    console.log(type + ' device does not exist');
  }
}

// Figure out the controls: amixer scontrols?
// Figure out controls from node-asound
nodeAsound.getPlaybackDevices(function(playbackDevices, err) {
  if (err) {
    console.log(err);
  }
  nodeAsound.getRecordingDevices(function(recordingDevices, err) {
    if (err) {
      console.log(err);
    }
    // Find the sound devices we want
    var playbackDevice = null;
    var recordingDevice = null;
    playbackDevices.forEach(function(device) {
      if (device.getName() == 'snd_rpi_hifiberry_dacplus') {
        playbackDevice = device;
      }
    });
    recordingDevices.forEach(function(device) {
      if (device.getName().indexOf('USB') !== -1) {
        recordingDevice = device;
      }
    });
    displayDevice('Playback', playbackDevice);
    displayDevice('Recording', recordingDevice);
//     fs.writeFileSync('/etc/asound.conf', 'defaults.pcm.card 1\n\
// defaults.ctl.card 1\n\
// ');
// fs.writeFileSync('/etc/asound.conf', 'defaults.pcm.card ' + playbackDevice.getCardNumber() + '\n\
// defaults.ctl.card ' + playbackDevice.getCardNumber() + '\n\
// ');
  });
});

var queue = [];
var processing = false;

function announce(text) {
  queue.push(text);
  processQueue();
}

function processQueue() {
  if (processing || !tts) { return; }
  processing = true;
  while (queue.length > 0) {
    var text = queue.shift();
    console.log("I'm going to say \"" + text + '"');
    tts.say(text, '/tmp/tts.wav', function(err) {
      if (err) {
        console.error(err);
      } else {
        console.log('Transcoding');
        var transcode = child_process.exec('sox /tmp/tts.wav -c 2 -r 44100 /tmp/speech.wav', function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            console.log(stdout);
            console.log(stderr);
          } else {
            console.log('Transcode complete, playing...');
            //fs.unlink('/tmp/tts.wav', function() {});
            var music = new Sound();
            music.play('/tmp/speech.wav');
            music.on('complete', function () {
              console.log('Play complete');
              //fs.unlink('/tmp/speech.wav', function() {});
            });
          }
        })
      }
    });
  }
  processing = false;
}

module.exports = announce;
