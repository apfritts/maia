import platform

import talkey


class Announcer:
    __queue = []
    __processing = False
    __tts = None

    @staticmethod
    def announce(text):
        Announcer.__queue.append(text)
        Announcer.__processQueue()

    @staticmethod
    def init():
        if platform.system() == 'Darwin':
            Announcer.__tts = talkey.Talkey(engine_preference=['say'])
        else:
            Announcer.__tts = talkey.Talkey(
                engine_preference=['espeak'],
                preferred_languages=['en'],
                espeak={
                    # Specify the engine options:
                    'options': {
                        'enabled': True,
                        'espeak': r'espeak',
                    },

                    # Specify some default voice options
                    'defaults': {
                        'words_per_minute': 150,
                        'variant': 'f4',
                    },
                })
        Announcer.__processQueue()

    @staticmethod
    def __processQueue():
        if Announcer.__processing or Announcer.__tts is None:
            return
        Announcer.__processing = True

        while len(Announcer.__queue) > 0:
            text = Announcer.__queue.pop(0)
            Announcer.__tts.say(text)

        Announcer.__processing = False

Announcer.init()
