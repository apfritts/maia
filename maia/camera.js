var fs = require('fs');
var RaspiCam = require('raspicam');

var photoLocation = '/data/image.jpg';
var camera = new RaspiCam({
  mode: 'photo',
  output: photoLocation,
  encoding: 'jpg',
  timeout: 100
});

camera.on('started', function( err, timestamp ){
  console.log('photo started at ' + timestamp );
});

camera.on('read', function( err, timestamp, filename ){
  console.log('photo image captured with filename: ' + filename );
});

camera.on('exit', function( timestamp ){
  console.log('photo child process has exited at ' + timestamp );
});

module.exports = {
  image: function(res) {
    var img = fs.readFileSync(photoLocation);
    res.writeHead(200, {'Content-Type': 'image/jpeg' });
    res.end(img, 'binary');
  },

  snap: function() {
    camera.start();
  },
};
