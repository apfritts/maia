import picamera

class Camera:
    def getImageLocation(self):
        return '/data/image.jpg'

    def snap(self):
        with picamera.PiCamera() as camera:
            camera.resolution = (2592, 1944)
            camera.capture(self.getImageLocation())
    