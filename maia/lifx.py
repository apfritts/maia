from lifxlan import LifxLAN

# https://github.com/mclarkk/lifxlan

class Lifx:
	__lights = None

	def init(self):
		lifx = LifxLAN()
		print("Looking for lights...")
		self.__lights = lifx.get_lights()
		print("\tFound {} lights".format(len(self.__lights)))

	def list(self):
		id = 0
		while id < len(self.__lights):
			light = self.__lights[id]
			print("{}: {}".format(id, light.get_label()))
			id = id + 1

	def activate(self, light):
		light.set_power(True)

if __name__ == '__main__':
	lifx = Lifx()
	lifx.init()
	lifx.list()
	#lifx = LifxLAN()
	#lights = lifx.get_lights()
	#print("\tFound {} lights".format(len(lights)))
	#id = 0
	#while id < len(lights):
	#		light = lights[id]
	#		print("{}: {}".format(id, light.get_label()))
	#		id = id + 1

