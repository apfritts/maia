'use strict';

var async = require('async');
var fs = require('fs');
var ini = require('ini');
var exec = require('child_process').exec;
var persist = require('./persist');

module.exports = {
  start: function() {
    var mopidy = ini.parse(fs.readFileSync('/etc/mopidy/mopidy.conf', 'utf-8'));

    // Make necessary directories
    var directories = [
      mopidy.core.cache_dir,
      mopidy.core.config_dir,
      mopidy.core.data_dir,
      mopidy.local.data_dir,
      mopidy.local.media_dir,
      mopidy.m3u.playlists_dir,
      mopidy.spotmop.artworklocation
    ];
    async.eachSeries(directories, function(dir, cb) {
      fs.mkdir(dir, cb);
    }, function() {
      // http config
      mopidy.http.port = persist.get('mopidy.http.port', '8080');

      // mpd config
      mopidy.mpd.port = persist.get('mopidy.mpd.port', '6680');

      // audio config
      mopidy.audio.mixer_volume = persist.get('mopidy.audio.mixer_volume', '50');

      // Google Play Music config
      mopidy.gmusic.enabled = persist.get('mopidy.gmusic.enabled', 'false');
      mopidy.gmusic.username = persist.get('mopidy.gmusic.username', 'none');
      mopidy.gmusic.password = persist.get('mopidy.gmusic.password', 'none');
      mopidy.gmusic.all_access = persist.get('mopidy.gmusic.all_access', 'false');

      // Spotify config
      mopidy.spotify.enabled = persist.get('mopidy.spotify.enabled', 'false');
      mopidy.spotify.username = persist.get('mopidy.spotify.username', 'none');
      mopidy.spotify.password = persist.get('mopidy.spotify.password', 'none');

      // Soundcloud config
      mopidy.soundcloud.enabled = persist.get('mopidy.soundcloud.enabled', 'false');
      mopidy.soundcloud.auth_token = persist.get('mopidy.soundcloud.auth_token', 'none');

      // Soundcloud config
      mopidy.youtube.enabled = persist.get('mopidy.youtube.enabled', 'false');

      fs.writeFileSync('/etc/mopidy/mopidy.conf', ini.stringify(mopidy));
      console.log('starting Mopidy - htttp port:' + mopidy.http.port + '; MPD port:' + mopidy.mpd.port);
      exec('systemctl start mopidy', function(err, stdout, stderr) {
        if (err) {
          console.log('Mopidy fatal error!');
          console.log(err);
          return;
        }
        console.log('Mopidy has started');
        console.log(stdout);
        console.log(stderr);
      });
    });
  },
};
