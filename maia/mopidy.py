from . import persist

import pprint
import subprocess


class Mopidy:
    def start():
        mopidy.http.port = persist.get('mopidy.http.port', '8080')

        # mpd config
        mopidy.mpd.port = persist.get('mopidy.mpd.port', '6680')

        # audio config
        mopidy.audio.mixer_volume = persist.get('mopidy.audio.mixer_volume', '50')

        # Google Play Music config
        mopidy.gmusic.enabled = persist.get('mopidy.gmusic.enabled', 'false')
        mopidy.gmusic.username = persist.get('mopidy.gmusic.username', 'none')
        mopidy.gmusic.password = persist.get('mopidy.gmusic.password', 'none')
        mopidy.gmusic.all_access = persist.get('mopidy.gmusic.all_access', 'false')

        # Spotify config
        mopidy.spotify.enabled = persist.get('mopidy.spotify.enabled', 'false')
        mopidy.spotify.username = persist.get('mopidy.spotify.username', 'none')
        mopidy.spotify.password = persist.get('mopidy.spotify.password', 'none')

        # Soundcloud config
        mopidy.soundcloud.enabled = persist.get('mopidy.soundcloud.enabled', 'false')
        mopidy.soundcloud.auth_token = persist.get('mopidy.soundcloud.auth_token', 'none')

        # Soundcloud config
        mopidy.youtube.enabled = persist.get('mopidy.youtube.enabled', 'false')

        fs.writeFileSync('/etc/mopidy/mopidy.conf', ini.stringify(mopidy));
        print('starting Mopidy - htttp port:' + mopidy.http.port + '; MPD port:' + mopidy.mpd.port)
        subprocess.cmd('systemctl start mopidy', stdout, stderr)
        if stderr is not None:
            print('Mopidy fatal error!')
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(err)
            return
        print('Mopidy has started')
        print(stdout)
        print(stderr)