var fs = require('fs');

var confFile = '/data/persist.json';
var conf = {};
if (fs.statSync(confFile).isFile()) {
  conf = JSON.parse(fs.readFileSync(confFile));
}

module.exports = {
  get: function(name, default_) {
    if (conf[name]) {
      console.log('Returning conf[' + name + ']');
      return conf[name]
    }
    console.log('Returning default for conf[' + name + ']');
    return default_;
  },

  set: function(name, value, callback) {
    console.log('Saving conf[' + name + ']');
    conf[name] = value;
    fs.writeFile(confFile, JSON.stringify(conf), callback);
  },
};
