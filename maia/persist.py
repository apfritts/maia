import json


class Persist:
    __config = None

    def get(name, default=None):
        Persist.__loadConfig()
        if name in __config:
            return __config[name]
        return default

    def set(name, value):
        Persist.__loadConfig()
        __config[name] = value
        Persist.__saveConfig()

    def __loadConfig():
        if __config is not None:
            return

    def __saveConfig():
        if __config is None:
            return

