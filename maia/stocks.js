var announcer = require('./announcer');
var async = require('async');
var googleStocks = require('google-stocks');

var stock1 = (process.env['STOCK_1'] || 'BOX,Box Inc').split(',');
var stock2 = (process.env['STOCK_2'] || 'GOOG,Google Inc').split(',');
var stock3 = (process.env['STOCK_3'] || 'AAPL,Apple Computers').split(',');

var interval = process.env['INTERVAL'] || 60

var stockList = {}
stockList[stock1[0]] = stock1[1];
stockList[stock2[0]] = stock2[1];
stockList[stock3[0]] = stock3[1];

module.exports = function(callback) {
  var stockPrices = '';
  async.eachOfSeries(stockList, function(stockName, stockCode, cb) {
    googleStocks([stockCode], function(error, data) {
      var text = stockName + ' stock is currently priced at ' + data[0]['l'] + '  dollars.';
      stockPrices += text + "\n";
      cb();
    });
  }, function(err) {
    if (err) {
      console.log(err);
      callback('Error reading stocks.');
    } else {
      callback(stockPrices);
    }
  });
};
