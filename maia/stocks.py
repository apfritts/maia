from . import announcer

import googlestocks


stocks = {
    'BOX': 'Box Inc',
    'GOOG': 'Google Inc',
    'AAPL': 'Apple Computers'}

def stocks():
    stockPrices = ''
    for symbol in stocks.keys():
        result = googlestocks.getStock(symbol)
        stockPrices += stocks[symbol] + ' stock is currently priced at ' + result[0]['l'] + ' dollars. '

    announcer.Announcer.say(stockPrices)
