var announcer = require('./src/announcer');
var camera = require('./src/camera');
var express = require('express');
var mopidy = require('./src/mopidy');
var stocks = require('./src/stocks');

var app = express();

var header = '<a href="/">Home</a> | <a href="/snap">Snap</a> | <a href="/image">Image</a> | <a href="/stocks">Stocks</a><br /><br />';

// Setup our routes
app.get('/stocks', function (req, res) {
  stocks(function(priceList) {
    announcer(priceList);
    res.send(header + priceList);
  });
});
app.get('/image', function (req, res) {
  res.send(header + '<img src="/image-raw"/>');
});
app.get('/image-raw', function (req, res) {
  camera.image(res);
});
app.get('/snap', function (req, res) {
  res.send(header + 'Snap!');
  camera.snap();
});
app.get('/', function (req, res) {
  res.send(header + 'Hello World!');
});

//start a server on port 80 and log its start to our console
var server = app.listen(80, function () {
  var port = server.address().port;
  announcer('Listening on port ' + port);
  mopidy.start();
});
