#!/bin/bash

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
  modprobe bcm2835_v4l2
fi

cd www
export FLASK_APP=app.py
export FLASK_DEBUG=1
export PYTHONPACKAGES=./:$PYTHONPACKAGES
flask run --host=0.0.0.0 --port=80
exit $?
