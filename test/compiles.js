var assert = require('chai').assert;

describe('application', function() {
  it('compiles', function() {
    assert.isOk(require('../server'));
    assert.isOk(require('../src/announcer'));
    assert.isOk(require('../src/camera'));
    assert.isOk(require('../src/mopidy'));
    assert.isOk(require('../src/persist'));
    assert.isOk(require('../src/stocks'));
  });
});
