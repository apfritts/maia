from flask import Flask
from flask import render_template
from flask import send_file

import os,sys,inspect
parentdir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, parentdir)

import maia

app = Flask('Maia')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/image.jpg')
def image_render():
    imgLocation = maia.camera.Camera().getImageLocation()
    return send_file(imgLocation, mimetype='image/gif')

@app.route('/snap')
def snap():
    try:
        maia.camera.Camera().snap()
        return render_template('index.html')
    except (ImportError, OSError) as e:
        return render_template('snap_error.html', message='{0}'.format(e))

@app.route('/stocks')
def stocks():
    stocks = []
    return render_template('stocks.html', stocks=stocks)

@app.errorhandler(404)
def not_found(error):
    return render_template('index.html', error="Hmmm...that page wasn't found. Try a link instead ;)"), 404

announcer.Announcer.announce('Howdy! Flask is running!')
